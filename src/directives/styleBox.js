/**
 * Created by murzik on 14.02.16.
 */
import angular from 'angular';
export default angular.module('appSizeComponents', [])
.directive('styleBox', function (){
    return {
        restrict: 'E',
        scope: {
            sizes: '=',
            dbox:  '=',
            box: '='
        },
        controller: function ($scope, $element, $attrs) {
        },
        template:
        '<div class="sizeButtonGroup">' +
          '<div class="sizeButton" ' +
               'ng-repeat="size in box.sizes">' +
               '<button class="btn btn-default btn-xs" ng-click="dbox.setBoxSize(size.x, size.y,size.z); box.setBoxSize(size.x, size.y,size.z)">{{size.x}} * {{size.y}} * {{size.z}}</button>' +
          '</div>' +
        '</div>'
    }
})