import angular from 'angular';

export default angular.module('appColorComponents',[])
  .directive('colorBox', function() {
       return {
            restrict: 'E',
            scope: {
                colors: '=',
                dbox:  '=',
                box:  '='
            },
           controller: function($scope) {
               //$scope.html2hex = function html2hex(html) {
               //    return html;
               //    //return html.replace('#', '0x');
               //};
               $scope.setColor = function (color) {
                   $scope.dbox.setBoxColor(color)
               }
           },
            template:
            '<div class="colorsGroup">' +
                '<div class="headColorText">' +
                    '<Button class="buttonBox btn btn-default btn-xs" ng-click="box.colorFor = \'box\'; dbox.colorFor = \'box\'">Box</Button>' +
                    '<Button class="buttonText btn btn-default btn-xs" ng-click="box.colorFor = \'text\'; dbox.colorFor = \'text\'">Text</Button>' +
                '</div>' +
                '<div>' +
                    '<div id="colorsDiv">' +
                        '<div ' +
                            'ng-repeat="color in box.colors"' +
                            'class="colorDiv"' +
                            'style="background-color: {{color}}"' +
                            'ng-click="dbox.setColor(color, box.setBoxColor(color))"'+
                            '&nbsp;' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>'
       }
  })