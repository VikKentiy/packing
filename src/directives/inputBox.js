/**
 * Created by samoylova on 09.03.16.
 */
import angular from 'angular';

export default angular.module('appInputComponents',[])
    .directive('inputBox', function() {
        return {
            restrict: 'E',
            scope: {
                colors: '=',
                dbox:  '=',
                table:  '='
            },
            controller: function($scope) {
                $scope.setCompanyText = function(text) {
                    $scope.dbox.setCompany(text)
                };
                $scope.setWebsiteText = function(text) {
                    $scope.dbox.setWebsite(text)
                };
                $scope.setSloganText = function(text) {
                    $scope.dbox.setSlogan(text)
                }
            },
            template:
                '<ul class="textInpGroup">' +
                    '<li>' +
                        '<input type="text" placeholder="Company" ng-model="table.company" ng-change="setCompanyText(table.company)" >' +
                    '</li>' +
                    '<li>' +
                        '<input type="text" placeholder="Website" ng-model="table.website" ng-change="setWebsiteText(table.website)">' +
                    '</li>' +
                    '<li>' +
                        '<input type="text" placeholder="Slogan" ng-model="table.slogan" ng-change="setSloganText(table.slogan)">' +
                    '</li>' +
                '</ul>'
        }
    })
