/**
 * Created by samoylova on 18.02.16.
 */
import angular from 'angular';

export default angular.module('appTableComponents', [])
.directive('tableBox', function () {
        return {
            restrict: 'E',
            $scope: {
                sizes: '=',
                box: '='
            },
            template:
              '<table>' +
                '<thead>Box params</thead>' +
                '<tbody>' +
                  '<tr>' +
                    '<td><strong>size</strong> {{appCtrl.box.box.width}} * ' +
                    '{{appCtrl.box.box.height}} * ' +
                    '{{appCtrl.box.box.depth}}</td>' +
                  '</tr>' +
                  '<tr>' +
                    '<td><strong>Company</strong> {{appCtrl.table.company}}</td>' +
                  '</tr>' +
                  '<tr>' +
                    '<td><strong>Website</strong> {{appCtrl.table.website}}</td>' +
                  '</tr>' +
                  '<tr>' +
                    '<td><strong>Slogan</strong> {{appCtrl.table.slogan}}</td>' +
                  '</tr>' +
                  '<tr>' +
                    '<td><div class="color" style="background-color: {{appCtrl.box.box.color}}; color:{{appCtrl.box.box.textColor}}"> color </div></td>' +
                  '</tr>' +
                '</tbody>' +
              '</table>'
        }
    })