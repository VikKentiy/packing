import angular from 'angular';
import styleBox from './directives/styleBox';
import colorBox from './directives/colorBox';
import inputBox from './directives/inputBox';
import tableBox from './directives/tableBox';
import boxService from './services/boxService';
import Cube from './services/3dBox';

require('./bower_components/bootstrap/dist/css/bootstrap.css');
require('./css/style.css');

var app = angular.module('app', ['appSizeComponents',
                                 'appColorComponents',
                                 'appInputComponents',
                                 'appBoxComponents',
                                 'appBoxRenderer',
                                 'appTableComponents']);

app.controller('appController', ['box', 'dBox', function(box, dbox){
  this.current = 'style';
    //console.log('dbox',dbox);
  this.dbox = dbox;
    window.dbox = dbox;
  this.dbox.init();
  this.dbox.renderDown();
  this.dbox.renderRight();
  this.box = box;


  this.table = {
    size: '',
    company: '',
    website: '',
    slogan: '',
    color: ''
  };

    window.table = this.table;
}]);
