/**
 * Created by samoylova on 23.02.16.
 */

import THREE from "three";
import angular from 'angular';
import Tween from 'tween';
console.log('tween',Tween);
var THREEx = require('imports?THREE=three!exports?THREEx!threex.dynamictexture/threex.dynamictexture');
console.log(THREEx);
class BoxRenderer {

    init() {

        if(this.initialized) {
            return;
        }

        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera( 40,200/200, 0.1, 1000 );
        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setSize( 200, 200);
        this.renderer.setClearColor(0xc1ebf5);
        document.getElementById('box').appendChild(this.renderer.domElement);
        this.colorText = '#000000';

        this.colors = {};
        this.colors.colorZero = 'beige';
        this.colors.colorOne = 'yellow';
        this.colors.colorTwo = 'orange';
        this.colors.colorThree = 'red';
        this.colors.colorFour = 'white';
        this.colors.colorFive = 'brown';

        this.text = {};
        this.text.textZero = 'beige';
        this.text.textOne = 'yellow';
        this.text.textTwo = 'orange';
        this.text.textThree = 'red';
        this.text.textFour = 'white';
        this.text.textFive = 'brown';

//================= zero ==================================
        var dynamicTextureZero	= new THREEx.DynamicTexture(100,50);
        dynamicTextureZero.context.font	= "bolder 15px Sans";
        dynamicTextureZero.clear(this.colors.colorZero).drawText(this.text.textZero, undefined, 30, this.colorText);

//================= one ==================================
        var dynamicTextureOne	= new THREEx.DynamicTexture(100,50);
        dynamicTextureOne.context.font	= "bolder 15px Sans";
        dynamicTextureOne.clear(this.colors.colorOne).drawText(this.text.textOne, undefined, 30, this.colorText);

//================= two ==================================
        var dynamicTextureTwo	= new THREEx.DynamicTexture(100,50);
        //window.dynamicTextureTwo = dynamicTextureTwo;
        dynamicTextureTwo.context.font	= "bolder 15px Sans";
        dynamicTextureTwo.clear(this.colors.colorTwo).drawText(this.text.textTwo, undefined, 30, this.colorText);

//================= three ==================================
        var dynamicTextureThree	= new THREEx.DynamicTexture(100,50);
        dynamicTextureThree.context.font	= "bolder 15px Sans";
        dynamicTextureThree.clear(this.colors.colorThree).drawText(this.text.textThree, undefined, 30, this.colorText);

//================= four ==================================
        var dynamicTextureFour	= new THREEx.DynamicTexture(100,50);
        dynamicTextureFour.context.font	= "bolder 15px Sans";
        dynamicTextureFour.clear(this.colors.colorFour).drawText(this.text.textFour, undefined, 30, this.colorText);

//================= five ==================================
        var dynamicTextureFive	= new THREEx.DynamicTexture(100,50);
        dynamicTextureFive.context.font	= "bolder 15px Sans";
        dynamicTextureFive.clear(this.colors.colorFive).drawText(this.text.textFive, undefined, 30, this.colorText);


        this.sideTextures = {};
        this.sideTextures.RIGHT_SIDE = dynamicTextureZero;
        this.sideTextures.LEFT_SIDE = dynamicTextureOne;
        this.sideTextures.TOP_SIDE = dynamicTextureTwo;
        this.sideTextures.BOTTOM_SIDE = dynamicTextureThree;
        this.sideTextures.FRONT_SIDE = dynamicTextureFour;
        this.sideTextures.BACK_SIDE = dynamicTextureFive;
        this.geometry = new THREE.BoxGeometry( 1.15, 0.7, 1 );
        this.materials = [
            new THREE.MeshBasicMaterial({
                    side:THREE.DoubleSide,
                    map: dynamicTextureZero.texture
            }),
            new THREE.MeshBasicMaterial({
                    side:THREE.DoubleSide,
                    map: dynamicTextureOne.texture
            }),
            new THREE.MeshBasicMaterial({
                    side:THREE.DoubleSide,
                    map: dynamicTextureTwo.texture
            }),
            new THREE.MeshBasicMaterial({
                    side:THREE.DoubleSide,
                    map: dynamicTextureThree.texture
            }),
            new THREE.MeshBasicMaterial({
                    side:THREE.DoubleSide,
                    map: dynamicTextureFour.texture
            }),
            new THREE.MeshBasicMaterial({
                    side:THREE.DoubleSide,
                    map: dynamicTextureFive.texture
            })
        ];

        this.material = new THREE.MultiMaterial(this.materials);

        this.cube = new THREE.Mesh(this.geometry, this.material );
        this.scene.add( this.cube );
        this.edges = new THREE.EdgesHelper(this.cube, 0x000000 );
        this.scene.add(this.edges);
        this.camera.position.z = 3;
        this.initialized = true;
        this.colorFor = "box";

        this.deg2rad = function (angle) {
            return angle *Math.PI / 180;
        }
    }


    setColor(color) {
        if('box' == this.colorFor) {

            this.colors.colorZero = color;
            this.colors.colorOne = color;
            this.colors.colorTwo = color;
            this.colors.colorThree = color;
            this.colors.colorFour = color;
            this.colors.colorFive = color;

            this.sideTextures.TOP_SIDE.clear(this.colors.colorTwo).drawText(this.text.textTwo, undefined, 30,'black');
            this.sideTextures.BOTTOM_SIDE.clear(this.colors.colorThree).drawText(this.text.textThree, undefined, 30,'black');
            this.sideTextures.FRONT_SIDE.clear(this.colors.colorFour).drawText(this.text.textFour, undefined, 30,'black');
            this.sideTextures.BACK_SIDE.clear(this.colors.colorFive).drawText(this.text.textFive, undefined, 30,'black');
            this.sideTextures.RIGHT_SIDE.clear(this.colors.colorZero).drawText(this.text.textZero, undefined, 30,'black');
            this.sideTextures.LEFT_SIDE.clear(this.colors.colorOne).drawText(this.text.textOne, undefined, 30,'black');
            this.renderer.render(this.scene, this.camera);
        } else if ('text' == this.colorFor){
            this.colorText = color;
            this.sideTextures.TOP_SIDE.clear(this.colors.colorTwo).drawText(this.text.textTwo, undefined, 30, color);
            this.sideTextures.BOTTOM_SIDE.clear(this.colors.colorThree).drawText(this.text.textThree, undefined, 30, color);
            this.sideTextures.FRONT_SIDE.clear(this.colors.colorFour).drawText(this.text.textTwo, undefined, 30, color);
            this.sideTextures.BACK_SIDE.clear(this.colors.colorFive).drawText(this.text.textThree, undefined, 30, color);
            this.sideTextures.RIGHT_SIDE.clear(this.colors.colorZero).drawText(this.text.textZero, undefined, 30, color);
            this.sideTextures.LEFT_SIDE.clear(this.colors.colorOne).drawText(this.text.textOne, undefined, 30, color);
            this.renderer.render(this.scene, this.camera);
        }
    }

    setBoxSize(x, y, z){
        this.cube.scale.set(x,y,z);
        this.renderer.render(this.scene, this.camera);
    }

    setCompany(text) {
        this.text.textTwo = text;
        this.text.textThree = '';
        this.sideTextures.TOP_SIDE.clear(this.colors.colorTwo).drawText(text, undefined, 30,'black');
        this.renderer.render(this.scene, this.camera);
    }
    setWebsite(text) {
        this.text.textFour = text;
        this.text.textFive = text;
        this.sideTextures.FRONT_SIDE.clear(this.colors.colorFour).drawText(text, undefined, 30,'black');
        this.sideTextures.BACK_SIDE.clear(this.colors.colorFive).drawText(text, undefined, 30,'black');
        this.renderer.render(this.scene, this.camera);
    }
    setSlogan(text) {
        this.text.textZero = text;
        this.text.textOne = text;
        this.sideTextures.RIGHT_SIDE.clear(this.colors.colorZero).drawText(text, undefined, 30,'black');
        this.sideTextures.LEFT_SIDE.clear(this.colors.colorOne).drawText(text, undefined, 30,'black');
        this.renderer.render(this.scene, this.camera);
    }

    renderDown () {
        this.cube.rotation.x += this.deg2rad(45);
        this.renderer.render(this.scene, this.camera);
    }
    renderUp () {
        this.cube.rotation.x -= this.deg2rad(45);
        this.renderer.render(this.scene, this.camera);
    }
    renderLeft () {
        this.cube.rotation.y += this.deg2rad(45);
        this.renderer.render(this.scene, this.camera);
    }
    renderRight () {
        this.cube.rotation.y -= this.deg2rad(45);
        this.renderer.render(this.scene, this.camera);
    }
}

export default angular.module('appBoxRenderer',['appBoxComponents']).service('dBox', BoxRenderer);
