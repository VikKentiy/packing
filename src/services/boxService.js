import angular from 'angular';
class BoxComponent {
    constructor(dBox) {
        this.box = {
            width : 0,
            height: 0,
            depth : 0,
            xLabel: '',
            ylabel: '',
            zLabel: '',
            color :'#FFF',
            textColor: '#000'
        };

        this.colorFor = 'box';

        this.sizes = [
            {
                x: 1.15,
                y: 0.7,
                z: 1
            },
            {
                x: 1,
                y: 0.6,
                z: 0.8
            },
            {
                x: 1.35,
                y: 0.5,
                z: 1
            },
            {
                x: 0.9,
                y: 1,
                z: 0.7
            }

        ];

        this.colors = [
            '#4FC1E9',
            '#FFFFFF',
            '#656D78',
            '#000000',
            '#A0D468',
            '#FFCE54',
            '#ED5565',
            '#EC87C0'
        ];
    }
    setBoxSize(x,y,z) {
        this.box.width = x;
        this.box.height = y;
        this.box.depth = z;
    }

    setBoxColor(color) {

        if('box' == this.colorFor) {
            this.box.color = color;
        } else if('text' == this.colorFor) {
            this.box.textColor = color;
        } else {
            throw new Exception("Wrong color destination")
        }
    }
}
export default angular.module('appBoxComponents', [])
.service('box', BoxComponent);